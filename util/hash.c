#include <stdio.h>
#include <ctype.h>

#define NUMERO_PRIMO 2011

int hash(char *c){
	char *p;
	unsigned h = 0, g;
	for (p = c; *p != '\0'; p = p + 1){
		h = (h << 4) + toupper(*p);
		if ((g = h&0xf0000000)) {
			h = h ^ (g >> 4);
			h = h ^g;
		}
	}
	return h % NUMERO_PRIMO;
}

int main() {
	printf("%d\n", hash("teste"));
	return 0;
}
