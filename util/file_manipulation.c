#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

char *strip_copy(const char *s) {
	char *p = malloc(strlen(s) + 1);
	if (p) {
		char *p2 = p;
		while (*s != '\0') {
			if (*s != '\t' && *s != '\n') *p2++ = *s++;
			else ++s;
		}
		*p2 = '\0';
	}
	return p;
}

unsigned int get_num_files(FILE *fp) {
    unsigned short int i, buffer, num_files;

    for (i = 0; ; i++) {
        buffer = fgetc(fp);
        if (buffer == 0xA) break; //Enquanto nao quebrar a linha
        if (i != 0) num_files = num_files * 10 + buffer - 48;
        else num_files = num_files + buffer - 48;
    }
    return num_files;
}

char **get_name_files(FILE *fp, unsigned int num_files) {
	char **name_files;
	unsigned int i = 0;
	char *line = NULL;
	size_t len = 0;
	ssize_t read;
	
	name_files = (char **)malloc(num_files * sizeof(char *));
	for (i = 0; i < num_files; i++) {
		if ((read = getline(&line, &len, fp)) != -1) {
			name_files[i] = malloc(sizeof(char) * len);
			strcpy(name_files[i], line);
		}
	}
	if (line) free (line);
	
	return name_files;
}

char *read_word(FILE *fp) {
	char *word = malloc(20), *wordp = word;
	size_t lenmax = 20, len = lenmax;
	unsigned int c;
	
	if (!word) return NULL;
	
	for (;;) {
		c = fgetc(fp);
		if (isspace(c) || ispunct(c)) break;
		
		if(--len == 0) {
			len = lenmax;
			char *wordn = realloc(wordp, lenmax *= 2);
			if (!wordn) {
				free (wordp);
				return NULL;
			}
			word = wordn + (word - wordp);
			wordp = wordn;
		}
		if (isspace(*word++ = c) || ispunct(*word++ = c)) break;
	}
	*word = '\0';
	return wordp;
}

int main() {
	FILE *fp;
	char *file_name = malloc(sizeof(char) * 50); //to nem ai
	char **name_files;
	unsigned int i, num_files;

	printf("%s", "Nome do arquivo: ");
	scanf("%s", file_name);

	if (!(fp = fopen(file_name, "r"))) return -1;
	num_files = get_num_files(fp);
	name_files = get_name_files(fp, num_files);
	fclose(fp);
	
	for (i = 0; i < num_files; i++) {
		if (!(fp = fopen(strip_copy(&name_files[i][0]), "r"))) return -1;
		read_word(fp);
		fclose(fp);
	}
	return 0;
}
